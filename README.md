Hardware:

  - seven  8x8 LED matrix 788BS,
  - fourteen SN74HC595N shift registers,
  - Raspberry Pi 3B+
  
  ![alt text](media/IMG_20191227_102652.jpg "Title Text")
  ![Sample Video](media/VID_20191227_102453.mp4)
  
This project is a text scroller built with 8x8 led matrices and shift registers.  There are better and much easier ways to make a scrolling led display, but I wanted to learn how to use shift registers.

The text scroller program is named scroller.py.  The other programs are various tests of the LED array.  The codes in this repository were developed from code available at www.sunfounder.com and www.adeept.com.

Kicad files are in the pcb directory if you want to have a pcb made.  Routing was performed using Freeroute.  Note that the pcb has 8 LED matrices and 16 shift registers, unlike the breadboard prototype which has only 7 LED matrices.

Scroller is controllable over Bluetooth with android app in the android directory.  Load the project into android studio to install on your phone.  Run the py/server/btScroller.py program.

To enable bluetooth on the rpi:
```
root@raspberrypi:~# gedit /etc/systemd/system/dbus-org.bluez.service

[Unit]
Description=Bluetooth service
Documentation=man:bluetoothd(8)
ConditionPathIsDirectory=/sys/class/bluetooth

[Service]
Type=dbus
BusName=org.bluez
ExecStart=/usr/lib/bluetooth/bluetoothd -C
ExecStartPost=/usr/bin/sdptool add SP
NotifyAccess=main
#WatchdogSec=10
#Restart=on-failure
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
LimitNPROC=1
ProtectHome=true
ProtectSystem=full

[Install]
WantedBy=bluetooth.target
Alias=dbus-org.bluez.service


=======================================

root@raspberrypi:~# systemctl daemon-reload
root@raspberrypi:~# systemctl restart bluetooth

might have to do
root@raspberrypi:~# apt-get install bluetooth
root@raspberrypi:~# apt-get install python-bluez
```
![alt text](media/Screenshot_2020-12-09_21-18-12.png "RPi pins")
| PCB pin | RPi pin |
|--- | --- |
| 1 | 2 |
| 2 | 4 |
| 3 | 12 |
| 4 | 11 |
| 5 | 36 |
| 6 | 13 |
| 7 | 14 |

To run the daemon:
cd py/server/
python btScroller.py
This program will listen via bluetooth

To run the scroller without the daemon:

cd py/scroller
python scroller.py


'''
Display same letter on each 8x8 LED matrix
'''

#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import charset5x8

SDI   = 13
RCLK  = 12
SRCLK = 11
MR    = 36
tval  = 0.00001


#these are the column masks fe grounds only column 1, others are high
per_line = [0xfe, 0xfd, 0xfb, 0xf7, 0xef, 0xdf, 0xbf, 0x7f]

def print_msg():
	print 'Program is running...'
	print 'Please press Ctrl+C to end the program...'

def setup():
	GPIO.setmode(GPIO.BOARD)    # Number GPIOs by its BCM location
	GPIO.setup(SDI, GPIO.OUT)
	GPIO.setup(RCLK, GPIO.OUT)
	GPIO.setup(SRCLK, GPIO.OUT)
	GPIO.setup(MR, GPIO.OUT)
	GPIO.output(SDI, GPIO.LOW)
	GPIO.output(RCLK, GPIO.LOW)
	GPIO.output(SRCLK, GPIO.LOW)
	GPIO.output(MR, GPIO.HIGH)

# Shift the data to 74HC595
def hc595_in(dat):
	for bit in range(0, 8):	
		#GPIO.output(SDI, 1 & (dat >> bit))
		GPIO.output(SDI, 0x80 & (dat << bit))
		GPIO.output(SRCLK, GPIO.HIGH)
		#time.sleep(0.000001)
		time.sleep(tval)
		GPIO.output(SRCLK, GPIO.LOW)

def hc595_out():
	GPIO.output(RCLK, GPIO.HIGH)
	time.sleep(tval)
	GPIO.output(RCLK, GPIO.LOW)

def flash(table):
	for i in range(8):
		if i > 4 :
			hc595_in(0x00);
		else:
			hc595_in(table[i])

		hc595_in(per_line[i])
		hc595_out()
		
	# Clean up last line
	hc595_in(0x00)
	hc595_in(per_line[7])
	hc595_out()

def show(table, second):
	start = time.time()
	while True:
		flash(table)
		finish = time.time()
		if finish - start > second:
			break

def main():
	charactors = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890!?,.<>'

	for table in charactors:
		show(charset5x8.character[table],2)
	resetLed()

def destroy():
	resetLed()	
	GPIO.cleanup()

def resetLed():
	for i in range(0,8):
		hc595_in(0x00)
		hc595_in(0xff)
		hc595_out()

if __name__ == '__main__':
	setup()
	try:
		main()
	except KeyboardInterrupt:
		destroy()

'''
Displays text 8x8 LED matrix array.
Letters are in correct orientation.
Text is in proper direction.
One blank column is between the letters.
One letter can span two 8x8 LED matrices.
'''

#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import charset5x8
import copy

numLeds = 7   # number of 8x8 led matrices connected
numColumns = 8 * numLeds # total number of columns in the matrix array
SDI   = 13
RCLK  = 12
SRCLK = 11
MR    = 36
tval  = 0.00000001


#these are the column masks fe grounds only column 1, others are high
per_line = [0xfe, 0xfd, 0xfb, 0xf7, 0xef, 0xdf, 0xbf, 0x7f]

def print_msg():
	print 'Program is running...'
	print 'Please press Ctrl+C to end the program...'

def setup():
	GPIO.setmode(GPIO.BOARD)    # Number GPIOs by its BCM location
	GPIO.setup(SDI, GPIO.OUT)
	GPIO.setup(RCLK, GPIO.OUT)
	GPIO.setup(SRCLK, GPIO.OUT)
	GPIO.setup(MR, GPIO.OUT)
	GPIO.output(SDI, GPIO.LOW)
	GPIO.output(RCLK, GPIO.LOW)
	GPIO.output(SRCLK, GPIO.LOW)
	GPIO.output(MR, GPIO.HIGH)

# Shift the data to 74HC595
def hc595_in(dat):
	for bit in range(0, 8):	
		#GPIO.output(SDI, 1 & (dat >> bit))
		GPIO.output(SDI, 0x80 & (dat << bit))
		GPIO.output(SRCLK, GPIO.HIGH)
		time.sleep(tval)
		GPIO.output(SRCLK, GPIO.LOW)

def hc595_out():
	GPIO.output(RCLK, GPIO.HIGH)
	time.sleep(tval)
	GPIO.output(RCLK, GPIO.LOW)

def flash(bytes):
	#each LED gets 8 bits of row data and
	#8 bits of column data
	for col in range(8):
		#send the 8 bits of row data and 8 bits of column
		#data to each LED
		for n in range(numLeds):
			# have to do 1 byte of column then row data
			index=numColumns - (n*8+col)-1
			#index=n*8+col
			if (index < len(bytes)):
				#print ("sending ", hex(bytes[index]), " to led ", n, " column ", col)
				hc595_in(bytes[index])
			else:
				#print ("sending 0x00 to led ", n, " column ", col)			
				hc595_in(0x00)
			hc595_in(per_line[index%8])

		#once all the bits have been sent to the entire 
		#array of n 8x8 leds, call hc595_out to set the data
		hc595_out()
		time.sleep(0.001)
	   	resetMR()
		
def show(bytes, second):
	print("calling show")

	start = time.time()
	while True:
		flash(bytes)
		
		finish = time.time()
		if finish - start > second:
			break

def resetMR():
	GPIO.output(MR, GPIO.LOW)
	hc595_out()
	GPIO.output(MR, GPIO.HIGH)
	
def main():
	#message = 'This is a test: AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890!?,.<>'
	message = 'abcde123456'
	
	#prepare the message byte array	
	messageBytes=[]
	for letter in message:
		charBytes=copy.deepcopy(charset5x8.character[letter])
		for i in range(len(charBytes)):
			messageBytes.append(charBytes[i])
		#add a blank column for space between the letters
		messageBytes.append(0x00)

	show(messageBytes,5)
						
	resetMR()

def destroy():
	resetMR()	
	GPIO.cleanup()

if __name__ == '__main__':
	setup()
	try:
		main()
	except KeyboardInterrupt:
		destroy()

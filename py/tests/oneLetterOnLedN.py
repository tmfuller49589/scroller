'''
Display one letter on one specified 8x8 LED matrix
'''

#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import charset5x8
import copy

numLeds = 7   # number of 8x8 led matrices connected
numColumns = 8 * numLeds # total number of columns in the matrix array
SDI   = 13
RCLK  = 12
SRCLK = 11
MR    = 36
tval  = 0.0000001
flval = 0.0000


#these are the column masks fe grounds only column 1, others are high
per_line = [0xfe, 0xfd, 0xfb, 0xf7, 0xef, 0xdf, 0xbf, 0x7f]

def print_msg():
	print 'Program is running...'
	print 'Please press Ctrl+C to end the program...'

def setup():
	GPIO.setmode(GPIO.BOARD)    # Number GPIOs by its BCM location
	GPIO.setup(SDI, GPIO.OUT)
	GPIO.setup(RCLK, GPIO.OUT)
	GPIO.setup(SRCLK, GPIO.OUT)
	GPIO.setup(MR, GPIO.OUT) 
	GPIO.output(SDI, GPIO.LOW)
	GPIO.output(RCLK, GPIO.LOW)
	GPIO.output(SRCLK, GPIO.LOW)
	GPIO.output(MR, GPIO.HIGH)

# Shift the data to 74HC595
def hc595_in(dat):
	for bit in range(0, 8):	
		#GPIO.output(SDI, 1 & (dat >> bit))
		GPIO.output(SDI, 0x80 & (dat << bit))
		GPIO.output(SRCLK, GPIO.HIGH)
		time.sleep(tval)
		GPIO.output(SRCLK, GPIO.LOW)

def hc595_out():
	GPIO.output(RCLK, GPIO.HIGH)
	time.sleep(tval)
	GPIO.output(RCLK, GPIO.LOW)

#n is the number of the 8x8 LED matrix in the matrix chain
#to display the letter on
def letterOnLed(n, bytes):
	
	for i in range(8):
		for xyz in range(n+1,numLeds):
			hc595_in(0x00)
			hc595_in(0xff)
		
		if (i < len(bytes)):
			hc595_in(bytes[i])
		else:
			hc595_in(0x00)
		hc595_in(per_line[i])
		
		for xyz in range(0,n):
			hc595_in(0x00)
			hc595_in(0xff)

		hc595_out()
		time.sleep(0.001)  #longer pause before clear = brighter letter
		resetMR()

def resetMR():
	GPIO.output(MR, GPIO.LOW)
	hc595_out()
	GPIO.output(MR, GPIO.HIGH)
	
def main():
	for i in range(0,1000):
		letterOnLed(6,charset5x8.character["e"])

def destroy():
	resetMR()	
	GPIO.cleanup()


if __name__ == '__main__':
	setup()
	try:
		main()
	except KeyboardInterrupt:
		destroy()

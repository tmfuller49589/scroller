#!/usr/bin/env python
'''
Illuminate entire 8x8 LED matrix.
Pause for 1 second.
Clear entire 8x8 LED matrix.
Repeat
'''

import RPi.GPIO as GPIO
import time

RCLK  = 12
SRCLK = 11
SDI   = 13
MR    = 36


def print_msg():
	print 'Program is running...'
	print 'Please press Ctrl+C to end the program...'

def setup():
	GPIO.setmode(GPIO.BOARD)    # Number GPIOs by its physical location
	GPIO.setup(MR, GPIO.OUT)
	GPIO.setup(SDI, GPIO.OUT)
	GPIO.setup(RCLK, GPIO.OUT)
	GPIO.setup(SRCLK, GPIO.OUT)
	GPIO.output(SDI, GPIO.LOW)
	GPIO.output(RCLK, GPIO.LOW)
	GPIO.output(SRCLK, GPIO.LOW)
	GPIO.output(MR, GPIO.HIGH)

def hc595_in(dat):
	for bit in range(0, 8):	
		GPIO.output(SDI, 0x80 & (dat << bit))
		GPIO.output(SRCLK, GPIO.HIGH)
		time.sleep(0.001)
		GPIO.output(SRCLK, GPIO.LOW)

def hc595_out():
	GPIO.output(RCLK, GPIO.HIGH)
	time.sleep(0.001)
	GPIO.output(RCLK, GPIO.LOW)


def loop():
	while True:
		for i in range(0, 8):
			hc595_in(0xff)
			hc595_in(0x00)
			hc595_out()
			time.sleep(0.1)
			
		time.sleep(1)
		GPIO.output(MR, GPIO.LOW)
		time.sleep(0.1)
		GPIO.output(MR, GPIO.HIGH)
	
		

def destroy():   # When program ending, the function is executed. 
	print "destroying "
	GPIO.output(MR, GPIO.LOW)
	time.sleep(0.1)
	GPIO.output(MR, GPIO.HIGH)
	time.sleep(0.1)
	GPIO.output(MR, GPIO.LOW)
	GPIO.cleanup()

if __name__ == '__main__':   # Program starting from here 
	print_msg()
	setup() 
	try:
		loop()  
	except KeyboardInterrupt:  
		destroy()  

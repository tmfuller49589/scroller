#!/usr/bin/env python
'''
Illuminate some LEDs on each 8x8 LED matrix.
'''

import RPi.GPIO as GPIO
import time

SDI   = 13
RCLK  = 12
SRCLK = 11
MR    = 36


def print_msg():
	print 'Program is running...'
	print 'Please press Ctrl+C to end the program...'

def setup():
	GPIO.setmode(GPIO.BOARD)    # Number GPIOs by its physical location
	GPIO.setup(MR, GPIO.OUT)
	GPIO.setup(SDI, GPIO.OUT)
	GPIO.setup(RCLK, GPIO.OUT)
	GPIO.setup(SRCLK, GPIO.OUT)
	GPIO.output(SDI, GPIO.LOW)
	GPIO.output(RCLK, GPIO.LOW)
	GPIO.output(SRCLK, GPIO.LOW)
	GPIO.output(MR, GPIO.HIGH)

def hc595_in(dat):
	#print ("dat = " , dat)
	for bit in range(0, 8):	
		datum=0x80 & (dat << bit)
		#print("bit = ", bit , " datum = " , datum)
		GPIO.output(SDI, datum)
		GPIO.output(SRCLK, GPIO.HIGH)
		time.sleep(0.0001)
		GPIO.output(SRCLK, GPIO.LOW)

def hc595_out():
	GPIO.output(RCLK, GPIO.HIGH)
	time.sleep(0.001)
	GPIO.output(RCLK, GPIO.LOW)


def loop():	
	resetLed()
	#column
	#1  2  3  4  5  6  7  8
	#fe fd fb f7 ef df bf 7f
	for x in range(0,100):
		hc595_in(0x08)  
		hc595_in(0xfe)
		hc595_out()
		time.sleep(0.0001)
		hc595_in(0x10)  
		hc595_in(0xfd)
		hc595_out()
		time.sleep(0.0001)
		hc595_in(0x20)  
		hc595_in(0xfb)
		hc595_out()
		time.sleep(0.0001)
		
		
	destroy()
		
		
def resetLed():
	for i in range(0,8):
		hc595_in(0x00)
		hc595_in(0xff)
		hc595_out()
			

def destroy():   # When program ending, the function is executed. 
	print ("destroying" )
	resetLed()
	GPIO.cleanup()

if __name__ == '__main__':   # Program starting from here 
	print_msg()
	setup() 
	try:
		loop()  
	except KeyboardInterrupt:  
		destroy()  

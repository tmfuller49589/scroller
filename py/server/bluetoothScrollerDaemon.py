import bluetooth
import threading
import time
import logging
import re
import config

    
def daemon():
    while True:
        server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )

        port = 0
        server_sock.bind(("",port))
        server_sock.listen(1)
        print ("listening on port %d" % port)

        bluetooth.advertise_service( server_sock, "ScrollerController Service", config.uuid )

        client_sock,address = server_sock.accept()
        print ("Accepted connection from ",address)

        while True:
            try:
                data = client_sock.recv(1024)
                config.messageChanged=True
                config.message=data.decode("utf-8")
                x = re.sub("\n", "", config.message)
                x = re.sub("'", "", x)
                config.message=x
                print ("received [%s]" % x)
                if (config.message=="exit"):
                    print("got exit message")
                    client_sock.close()
                    server_sock.close()
                    config.loopFlag=False
                    return

            except bluetooth.btcommon.BluetoothError as e:
                print("caught error " , e)
                break
            
        client_sock.close()
        server_sock.close()


'''
Simple program to listen on a bluetooth port and
print out received data.
'''

import bluetooth
    
uuid="66caf30a-4c00-4cdb-a1eb-57a160a0a4a9"

server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )

port = 0
server_sock.bind(("",port))
server_sock.listen(1)
print ("listening on port %d" % port)

bluetooth.advertise_service( server_sock, "ScrollerController Service", uuid )

client_sock,address = server_sock.accept()
print ("Accepted connection from ",address)

while True:
	data = client_sock.recv(1024)
	print ("received [%s]" % data)
	if (data=="exit"):
		break

client_sock.close()
server_sock.close()

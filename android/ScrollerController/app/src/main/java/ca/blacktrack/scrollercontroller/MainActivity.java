package ca.blacktrack.scrollercontroller;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    public int REQUEST_ENABLE_BT=100;
    private final static String TAG="ScrollerController";
    private static BluetoothDevice scrollerBluetoothDevice=null;
    public static final java.util.UUID ScrollerControllerUUID =
            UUID.fromString("66caf30a-4c00-4cdb-a1eb-57a160a0a4a9");
    public static BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    BluetoothThread btt;

    public String address="";

    // Handler for writing messages to the Bluetooth connection
    Handler writeHandler;

    HashMap<String, String> nameAddressMap;

    public MainActivity() {

    }

    public static BluetoothDevice getScrollerBluetoothDevice() {
        return scrollerBluetoothDevice;
    }

    public static BluetoothAdapter getBluetoothAdapter() {
        return bluetoothAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final TextView textViewInput = findViewById(R.id.editText);
        TextView textViewOutput = findViewById(R.id.textViewOutput);
        Button sendButton = findViewById(R.id.sendButton);
        sendButton.setOnClickListener((view)-> {
            sendButtonPressed(view);
        });

        Button connectButton = findViewById(R.id.connectButton);
        connectButton.setOnClickListener((view)-> {
           connectButtonPressed(view);
        });

        Button disconnectButton = findViewById(R.id.disconnectButton);
        disconnectButton.setOnClickListener((view)-> {
            disconnectButtonPressed(view);
        });

    }


    public void initBluetooth() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Log.d(TAG, "BluetoothAdapter is " + bluetoothAdapter);
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            return;
        }
        Log.d(TAG, "Bluetooth enabled is " + bluetoothAdapter.isEnabled());

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a device");

        nameAddressMap = new HashMap<String, String>();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                nameAddressMap.put(deviceName, deviceHardwareAddress);

                Log.d(TAG, "deviceName is " + device.getName());

                Log.d(TAG, "address is " + device.getAddress());
                if (device.getAddress().equals(address)) {
                    Log.d(TAG, "Found correct device");
                }
            }
        }

        CharSequence[] addresses = new CharSequence[nameAddressMap.size()];
        int i=0;
        for (Map.Entry<String, String> entry: nameAddressMap.entrySet()) {
            addresses[i]=entry.getKey();
            Log.d(TAG, "adding " + addresses[i]);
            ++i;
        }

        builder.setItems(addresses, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                address = nameAddressMap.get(addresses[which]);
                Log.d(TAG, "picked address " + address);
                initBluetoothThread();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();

        dialog.show();

        Log.d(TAG, "made it here already");
    }

    private String getNameFromAddress(String address) {
        for (Map.Entry<String, String> entry : nameAddressMap.entrySet()) {
           if (entry.getValue().equals(address)) {
               return entry.getKey();
           }
        }
        return "";
    }
    private void initBluetoothThread() {
        // Only one thread at a time
        if (btt != null) {
            Log.d(TAG, "Already connected!");
            return;
        }

        // Initialize the Bluetooth thread, passing in a MAC address
        // and a Handler that will receive incoming messages
        btt = new BluetoothThread(address, new Handler() {

            @Override
            public void handleMessage(Message message) {

                String s = (String) message.obj;

                // Do something with the message
                if (s.equals("CONNECTED")) {
                    TextView tv =  findViewById(R.id.statusText);
                    tv.setText("Connected to " + address + " " + getNameFromAddress(address));
                    Button b =  findViewById(R.id.disconnectButton);
                    b.setEnabled(true);
                    b =  findViewById(R.id.connectButton);
                    b.setEnabled(false);
                } else if (s.equals("DISCONNECTED")) {
                    TextView tv =  findViewById(R.id.statusText);
                    Button b = findViewById(R.id.connectButton);
                    b.setEnabled(true);
                    b = findViewById(R.id.disconnectButton);
                    b.setEnabled(false);
                    tv.setText("Disconnected.");
                } else if (s.equals("CONNECTION FAILED")) {
                    TextView tv = findViewById(R.id.statusText);
                    tv.setText("Connection failed!");
                    btt = null;
                } else {
                    TextView tv =  findViewById(R.id.textViewOutput);
                    tv.setText(s);
                }
            }
        });

        // Get the handler that is used to send messages
        writeHandler = btt.getWriteHandler();

        // Run the thread
        btt.start();

        TextView tv = (TextView) findViewById(R.id.statusText);
        tv.setText("Connecting...");

    }

    /**
     * Launch the Bluetooth thread.
     */
    public void connectButtonPressed(View v) {
        Log.d(TAG, "Connect button pressed.");
        initBluetooth();
    }

    /**
     * Kill the Bluetooth thread.
     */
    public void disconnectButtonPressed(View v) {
        Log.v(TAG, "Disconnect button pressed.");

        if(btt != null) {
            btt.interrupt();
            btt = null;
        }
    }

    /**
     * Send a message using the Bluetooth thread's write handler.
     */
    public void sendButtonPressed(View v) {
        Log.v(TAG, "Send button pressed.");

        TextView tv = findViewById(R.id.textViewOutput);
        TextView editText = findViewById(R.id.editText);
        tv.setText("Sent -->" + editText.getText() + "<--");
        String data = editText.getText().toString();

        Message msg = Message.obtain();
        msg.obj = data;
        writeHandler.sendMessage(msg);
    }

    // not currently used
    protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        // Check which request we're responding to
        Log.d(TAG,"onActivityResult requestCode is " + requestCode + " result is " + resultCode);
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "result is OK");

                Uri stuff = resultIntent.getData();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
